# Disclaimer
This guide is largely inspired by another guide made by "Ataraxxia" named `secure-arch`, you can find it here: https://github.com/Ataraxxia/secure-arch/blob/main/00_basic_system_installation.md
Someone also made a video version of the aforementioned guide on YouTube: https://youtu.be/4xeNL7nJLrM (This one slightly alters from the original). 

I am not a professional and I do not work in a cyber security related field. Please take everything said in this guide with a grain of salt. For me to make a guide that's truly "secure", I would mostly likely need to conduct a team of professionals as well as take into account multiple threat models. 

Also please take a look at the Arch Linux security wiki page. (https://wiki.archlinux.org/title/Security)

If you don't know what your threat model is, I'd recommend reading this article by the Electronic Frontier Foundation (tm): https://ssd.eff.org/module/your-security-plan
# Warning
There are commands in this guide that you will need to read very carefully. If you edit them incorrectly, you could cause unrecoverable data loss, especially in the [Partitioning, provisioning, encryption.] section. Please double check every command before you run it. 
I am not responsible for any damage you may cause to your system.
# Assumptions and Requirements
Many parts of this guide require your system to be UEFI. I'm going to assume that you've went through the manual installation guide on the Arch wiki once or twice, if you haven't, I highly suggest doing so. I'm also going to assume that you have some experience with the command line. 

In summary:
- Your system needs to be UEFI 
- You should be able to use your own custom secure boot keys
- Have some experience installing Arch Linux
# Partitioning, provisioning, encryption. 
Read first before doing anything!
Once you've downloaded the Arch Installation media (And hopefully verified it with GPG), flashed it, booted into the Arch Linux live CD...

**Wiping the hardrive** 

This section is going to be highly dependent on how paranoid you are, (or what your threat model is) I would recommend overwriting your drvie with random data, but zero-filling it would be optimal unless you're running from a 3-letter agency or something.
To zero-fill:

```
dd if=/dev/zero of=/dev/your_target_drive bs=1M status=progress
```

(Optional) To overwrite with random data:
```
dd if=/dev/urandom of=/dev/your_target_drive bs=1M status=progress
```

If you use an SSD, don't do this unless you want to wear down your drive faster,  most UEFI/BIOS's have a secure erase feature, use that instead. If you're going to do this anyway, it can take hours depending on how fast and how big your drive is. For reference, a 256 GiB SATA SSD took me roughly an hour and a half to overwrite with just one pass. (5500+ seconds). 

**Partitioning**

You want 2 partitions, one for EFI and the other for LUKS. 
I'd suggest 300 MiB - 1G for the EFI partition and the rest of your drive for the LUKS one. If you're going to dualboot, make the LUKS partition smaller. I like to use an application like `cfdisk` to do this. 

**LUKS/LVM setup** 

Format your EFI system partition with FAT32: 
```
mkfs.fat -F 32 /dev/EFI_block_device
```

Format your LUKS partition with well... LUKS:

```
cryptsetup luksFormat /dev/crypt_LUKS_block_device
```

Now open that device: (Replace  device_name with anything you want)
```
cryptsetup luksOpen /dev/crypt_LUKS_block_device device_name
```

In this guide I will choose to configure the installation with the use of LVM or Logical-Volume-Management, you can choose not to do this and just format the device you opened, or format it with BTRFS and enable Subvolumes as well as compression. You can consult the Arch Wiki if you wish to do so. 

Create a physical LVM volume 
```
pvcreate /dev/mapper/device_name
```

Create a volume group named "vg0"
```
`vgcreate vg0 /dev/mapper/device_name`
```

Now you can start creating volumes for partitions you would like to mount (/, /home, /opt, /var, etc etc.) If you want everything to be stored inside of one root volume, you can fill up the entire volume group: 

```
lvcreate -l 100%FREE vg0 -n root
```

But, I'd recommend having seperate /home & swap volumes. Of course you could also have seperate /var, /opt, and other volumes and set appropriate sizes for all of them. 

For home: (Replace 100G with whatever size you want)

```
lvcreate -L 100G vg0 -n home
```

For SWAP:  SWAP usually needs to be double your RAM size, so if you have 8 GiB of RAM, you'd need 16 GiB of swap. Personally I have 16 GB of RAM and I don't need more than 16 GiB of SWAP anyways. 

```
lvcreate -L 16G vg0 -n swap
```

Fill the rest of the volume group with the root volume: 

```
lvcreate -l 100%FREE  vg0 -n root
```

Then format them, I'd recommend using either BTRFS (Good for SSD's) or EXT4. In this example, I'll format root with BTRFS and vg0-home with EXT4. 

Format volumes:
```
mkswap /dev/mapper/vg0-swap
mkfs.btrfs /dev/mapper/vg0-root
mkfs.ext4 /dev/mapper/vg0-home
```

You will also have to format any other volumes you made, I'd recommend just sticking to ext4.
# Bootstrap Phase
When you're done formatting all of your volumes, you'll now have to mount them.
First you need to mount your root volume on /mnt:

```
mount /dev/mapper/vg0-root /mnt
```

Then create folders corresponding to your EFI system partition and other volumes you may have created: 
```
mkdir -p /mnt/boot/efi /mnt/home
```

Then mount those volumes and also swapon any swap volumes you may have created:
```
mount /dev/mapper/vg0-home /mnt/home
mount /dev/EFI_block_device /mnt/boot/efi
swapon /dev/mapper/vg0-swap
```

Now you're ready to install the Arch Linux base system, along with other packages. Remember to connect to the internet. If you're using ethernet, you can just use `dhcpcd` to connect with DHCP. If you're connecting over wifi, the live CD has iwd and `iwctl` which you can use to connect to an access point using your wifi adapter. If your wifi adapter is named `wlan0` you can go into `iwctl` and type `station wlan0 connect <SSID/Name of access point>`
Type in your WPA2/3 password, and you should be connected. 

Here are the packages I'd recommend installing, notably; `linux-hardened` as the kernel, `networkmanager` for networking,  `sbsigntools`  for secureboot setup later, `neovim` for the text editor (Replace with something like `nano` if you don't know how to exit vim lol) `linux-firmware` for proprietary firmware blobs, etc. And also a UCODE package. If you use AMD install `amd-ucode`, for intel install `intel-ucode`.

```
pacstrap /mnt base base-devel linux-hardened linux-hardened-headers linux-firmware YOUR_UCODE_PACKAGE sudo neovim lvm2 dracut sbsigntools networkmanager git efibootmgr binutils dhcpcd`
``` 

You can now generate your FSTAB file which your system uses to mount filesystems.

```
genfstab -U /mnt >> /mnt/etc/fstab
```

You can also edit it to mount `/tmp` on tmpfs or RAM. Simply paste this line at the bottom of the file. Start by editing `/mnt/etc/fstab` and pasting in the following line at the bottom of the file:
```
# TMP over TMPFS
tmpfs /tmp tmpfs rw,nosuid,noatime,nodev,size=4G,mode=1777 0 0
```

# Configure & Chroot phase

Now you can configure your installation and make it bootable. 
Start by chrooting into it:

```
arch-chroot /mnt
```

The first thing you'd wanna do is lock the root account, typically you would never wanna use this account for daily usage, all of your administrative permissions should be achieved with sudo. 

```
passwd -l root
```

**Account Creation**
Now you can create your user accounts, I would recommend creating 2 of them, one as an "Administrator" account for handling package updates, installation, etc. And the other for daily desktop usage. 

The following command creates a user with a home directory,  in group `wheel`, with it's shell set to `/bin/bash`, named "Administrator_Account". You could also optionally remove the `-m` part so the user is created without a home directory. 

```
useradd -m -G wheel -s /bin/bash Administrator_Account
``` 

Then add a desktop user:
```
useradd -m -G video,audio -s /bin/bash larry
```

Remember to set strong individual passwords for both of them using `passwd` followed by their username.

Moving on from accounts, you can optionally download the Man pages if you ever get stuck and need to read a manual for a certain command: 

```
pacman -S man-db
```

Set your timezone (TZ) For example, if you're from Berlin, it would be Europe/Berlin

```
ln -sf /usr/share/zoneinfo/<Region>/<city> /etc/localtime
```

**Locale configuration**
Edit the /etc/locale.gen file and uncomment the locales you would like to generate. For English US, uncomment en_US.UTF-8 .
Then generate them:
```
locale-gen
```

Optionally you can also configure your console keyboard layout, the one which is used in the TTY for example. If you have a US layout, you can skip this part.  Simply edit the /etc/vconsole.conf file.

Edit your /etc/sudoers file, so your administrator account can use sudo. Uncomment the line portaining the wheel group. 
If neovim complains about the file being read-only, add a `!` to force the edit. so if you want to write & quit, it would be `:wq!`
**Networking** 

Enable these systemd units for networking: 

```
systemctl enable dhcpcd
systemctl enable NetworkManager
```

# Unified Kernel Image Setup

Why would you want to use a UKI? It's like an all-in-one image for your kernel, initramfs, etc etc. This can allow you to bypass the use of a bootloader and boot from UEFI directly. 

Setup scriptz for creating  & removing the image (Stolen from Ataraxxia)
`nvim /usr/local/bin/uki-install`

Add the following: 

```
#!/usr/bin/env bash

	mkdir -p /boot/efi/EFI/Linux

	while read -r line; do
		if [[ "$line" == 'usr/lib/modules/'+([^/])'/pkgbase' ]]; then
			kver="${line#'usr/lib/modules/'}"
			kver="${kver%'/pkgbase'}"
	
			dracut --force --uefi --kver "$kver" /boot/efi/EFI/Linux/arch-linux.efi
		fi
	done
```

`nvim /usr/local/bin/uki-remove`

Add the following:

```
#!/usr/bin/env bash
 	rm -f /boot/efi/EFI/Linux/arch-linux.efi
```

Make both of them executable:

```
chmod +x /usr/local/bin/uki-*`
```

Configure pacman hooks, so that when you upgrade your kernel, this will allow pacman automatically remove and create the UKI image. 

Create the hooks directory:

```
mkdir /etc/pacman.d/hooks
```

Create this file: 
`nvim /etc/pacman.d/hooks/90-dracut-install.hook`

Add the following: (Also stolen from Ataraxxia) 

```
[Trigger]
	Type = Path
	Operation = Install
	Operation = Upgrade
	Target = usr/lib/modules/*/pkgbase
	
	[Action]
	Description = Updating linux EFI image
	When = PostTransaction
	Exec = /usr/local/bin/uki-install
	Depends = dracut
	NeedsTargets
```

Create this file:

`nvim /etc/pacman.d/hooks/60-dracut-remove.hook`

Add the following: 
```
[Trigger]
	Type = Path
	Operation = Remove
	Target = usr/lib/modules/*/pkgbase
	
	[Action]
	Description = Removing linux EFI image
	When = PreTransaction
	Exec = /usr/local/bin/uki-remove
	NeedsTargets
```


In this next part, we will make the install & UKI bootable. 

We must configure dracut to pass the neccessary kernel command line parameters for `cryptsetup` and lvm to work properly. 

Add your LUKS container's UUID to this config file: 
```
blkid -s UUID -o value /dev/crypt_LUKS_block_device >> /etc/dracut.conf.d/cmdline.conf
```


Edit said file and add the following: 
```
kernel_cmdline="rd.luks.uuid=luks-YOUR_UUID rd.lvm.lv=vg0/root root=/dev/mapper/vg0-root rootfstype=btrfs rootflags=rw,relatime"
```



After you've replaced the "YOUR_UUID" with the UUID you put in the file earlier, remember to remove it so the only line in the file is the "kernel_cmdline".  Also remember to replace the rootfstype with whatever you formatted your root volume with. 

Set some dracut related flags in `/etc/dracut.conf.d/flags.conf`
Add the following:
```
compress="zstd"
hostonly="no"
```

At this point, you should re-install your kernel, so that the scripts and pacman hooks we created  generate  the UKI image using dracut, in our case it is linux-hardened. 

`pacman -S linux-hardened`

**This step could be platform dependent...**

Ataraxxia - in their guide, mentioned that some older platforms can ignore efibootmanager entries all together and look for `EFI\BOOT\bootx64.efi` instead,  in that case you may generate your UKI directly to that directory and under that name. It's very important that the name is also bootx64.efi.


Use `efibootmgr` to create a UEFI boot entry pointing to your UKI image:

```
efibootmgr --create --disk /dev/EFI_system_block_device --part 1 --label "Arch Linux" --loader 'EFI\Linux\arch-linux.efi' --unicode
```

Run `efibootmgr` again to check if the boot entry is actually there. 

Exit out of chroot with `exit`, unmount all of the filesystems, and...
You can now reboot :) 


# Secure Boot Setup
Assuming you're in setup mode, you can now create your keys, enroll them, sign your UKI image. 

First, install `sbctl` or Secure-Boot control. 
`pacman -S sbctl`

Create the keys:
`sbctl create-keys`

Sign your UKI image: 
`sbctl sign -s /boot/efi/EFI/Linux/arch-linux.efi`

Configure dracut to use uefi_secureboot
`nvim /etc/dracut.conf.d/secureboot.conf`

Add the following: 
```
uefi_secureboot_cert="/usr/share/secureboot/keys/db/db.pem"
uefi_secureboot_key="/usr/share/secureboot/keys/db/db.key"
```

Create a pacman hook to automatically sign a new UKI image: 
`nvim /etc/pacman.d/hooks/zz-sbctl.hook`

Add the following: 

```
[Trigger]
	Type = Path
	Operation = Install
	Operation = Upgrade
	Operation = Remove
	Target = boot/*
	Target = efi/*
	Target = usr/lib/modules/*/vmlinuz
	Target = usr/lib/initcpio/*
	Target = usr/lib/**/efi/*.efi*

	[Action]
	Description = Signing EFI binaries...
	When = PostTransaction
	Exec = /usr/bin/sbctl sign /boot/efi/EFI/Linux/arch-linux.efi
```
**Note:**
`sbctl` does NOT sign your images using Microsoft's own keys. It does not have access to Microsofts private keys, only the ones you created using `sbctl create-keys`. Do not double sign your binaries! This would only be useful if you're gonna sign files part of Microsoft's bootchain. In our case, we do not need to do this. 

A lot of platforms **require** you to append your keys with Microsoft's CA. Enroll your keys:

`sbctl enroll-keys --microsoft`

Some platforms could allow you to not append Microsoft CA's keys, although using the former command works just fine. Don't run both of these.

`sbctl enroll-keys`


You can now reboot to your UEFI / BIOS and enable secure boot. 

# AppArmor Setup
AppArmor allows you to sandbox your applications by creating 'profiles' for them to only allow them to do things they're supposed to and denying things they're not supposed to do. 

Why not use SELinux? I would use it, but currently, it's not fully supported yet, the main issue being that the Arch reference policy isn't complete and doesn't work that well. AppArmor is good for most usecases, and is easy to setup. 

For installation of AppArmor: 
First, append this line to your kernel command line parameters to the file we created earlier.
Paste this line in `/etc/dracut.conf.d/cmdline.conf` :

```
lsm=landlock,lockdown,yama,integrity,apparmor,bpf
```

Then enable the AppArmor service:

```
systemctl enable apparmor.service
```

Re-install your kernel so dracut appends those new kernel parameters in your UKI image:
`pacman -S linux-hardened`

Then reboot, you can verify apparmor is loaded and active by running `sudo aa-status` with your administrator account.

How do I use AppArmor? The concept is simple. It has several modes. `enforcing` , `complain` and `unconfined`

Enforcing means that the applications profile will allow things it's supposed to do and deny things it's not supposed to do, as per the defined profile. 
Complain mode means that the applications profile will allow everything specified and ***not*** deny actions that are not defined in the profile but will log them. 
Unconfined mode essentially that no actions are made. Everything is allowed and denied actions are not logged. 

In order to create a profile for a specific application, in this example let's say `ani-cli`, 
you want to run `sudo aa-genprof /usr/bin/ani-cli` in your administrator account. When you run this command, you want to start using your application. Make sure you do everything from searching to accessing specifc files if need-be. As you do this, you want to go back to the terminal you ran the` aa-genprof` command on and press S to scan for log changes, then choose what you want it to do (Keep in mind some stuff might be critical for functionality) and if it needs access to a certain file, I would just choose (I)nherit. When you're done, you can save the changes and Finish creating the profile, this will apply the profile in `enforce` mode. 

You could also create your profile manually in `/etc/apparmor.d/`, I've found a repository with a lot of these AppArmor profiles, keep in mind that [as of writing] it has not been updated since Jul 9 of 2023. https://github.com/krathalan/apparmor-profiles It's also available on the AUR, named "krathalan-apparmor-profile-git". 

You might wanna repeat this process for applications you use daily, and stick to another 
method of sandboxing for everything else. 


If your profile breaks the application in some way, you can disable it with `aa-disable`. Or to disable ALL profiles run `aa-teardown`. 
# Additional practices
Please read the Arch Linux security wiki page here: https://wiki.archlinux.org/title/Security 

**Less is more**

This is probably the easiest thing you could implement. Don't install meaningless crap you're never gonna use. Only install what is necessasry, keep applications to a minimum. Delete system caches, history, file history frequently, uninstall anything you might not be using anymore. The less applications you use, the smaller your attack surface is. (In theory anyway)

**Securely browsing the web**

I'd strongly suggest using a broswer with sensible defaults like Librewolf (https://librewolf.net/), it's available in the AUR as a binary. (`librewolf-bin`). You may also want to install several extensions to further enhance your privacy, although, remember that you should never install untrusted or closed-source extensions and keep them to a very strict minimum; 

- NoScript, don't allow websites to execute JavaScript and other functions without permission. 
- LocalCDN, emulates Content Delivery Networks to improve your online privacy.
- CanvasBlocker, allows for better protection against fingerprinting, from alright blocking to faking certain forms of fingerprinting.
- ClearURL's, will automatically remove tracking elements from URLs.

If you really wanna use a chromium based browser, I suggest `ungoogled-chromium`.

**USB device protection**

If you're even more concerned about physical security, you can setup `usbguard` (https://usbguard.github.io/) to block external and unrecognized USB devices as well as  `usbkill` which actively shuts down your computer when an unrecognised USB device is connected. 
Wiki page: https://wiki.archlinux.org/title/USBGuard

**ClamAV antivirus**

This is to be used to scan files for malware. I'd recommend doing this if you're going to install any and all software  from a third party. (Like a github script or executable file you downloaded from a website). You could also setup OnAccessScan to automatically scan files when they've been renamed, moved or created (AND OR) modified in some way.
Wiki page: https://wiki.archlinux.org/title/ClamAV

**Auditing your system with Lynis**

`lynis` is a tool that will analyze your system and give you tips on how to harden your system even further. It's main goals are:
- Automated security auditing
- Compliance testing (e.g. ISO27001, PCI-DSS, HIPAA)
- Vulnerability detection

You can also install `rkhunter`, which is designed to audit your system for possible rootkits. 

**Universal 2nd Factor (U2F)** 

Using U2F, you can use a USB security token (Like a Yubikey for example) to handle various authentication related tasks on your system. Like logging in with sudo, GDM, making use of the PAM module `pam-u2f`, OpenSSH, "Data-at-rest encryption" using LUKS. 
Wiki page: https://wiki.archlinux.org/title/Universal_2nd_Factor

**Advanced Intrusion Detection Environment (AIDE)**

AIDE Is a host-based intrusion detection system (HIDS) for checking the integrity of files. This can allow you to automatically detect if a file has been tampered with. AIDE **DOES NOT** check for rootkits or look through logfiles for suspicious activity (Like OSSEC for example). It can only check the integrity of files. 
Wiki page: https://wiki.archlinux.org/title/AIDE

**Further kernel hardening**

Simply using `linux-hardened` may not be enough. There are still things you could do, like setup DKMS to install LKRG which is avalidable in the AUR as `lkrg-dkms` (https://lkrg.org). Setup signed kernel modules. (https://wiki.archlinux.org/title/Signed_kernel_modules). 

You may also want to harden the kernel network stack. A minimal example of this would be to:

Create & edit `/etc/sysctl.d/90-network.conf`, add the following: (Stolen from Ataraxxia, https://github.com/Ataraxxia/secure-arch/blob/main/02_basic_hardening.md)

```
# Do not act as a router
net.ipv4.ip_forward = 0
net.ipv6.conf.all.forwarding = 0

# SYN flood protection
net.ipv4.tcp_syncookies = 1

# Disable ICMP redirect
net.ipv4.conf.all.accept_redirects = 0
net.ipv4.conf.default.accept_redirects = 0
net.ipv4.conf.all.secure_redirects = 0
net.ipv4.conf.default.secure_redirects = 0
net.ipv6.conf.all.accept_redirects = 0
net.ipv6.conf.default.accept_redirects = 0

# Do not send ICMP redirects
net.ipv4.conf.all.send_redirects = 0
net.ipv4.conf.default.send_redirects = 0
```
Then load these new rules with: 
```
sudo sysctl --system
```

# Closing Thoughts
You're basically ready to start using Arch Linux, like installing a Desktop Environent or WM. 
A couple of things to keep in mind, when you install an application, remember to install it using your administrator account and use it with your desktop account. I would also recommend using heavy use of sandboxing. So if you need to use a  web app like Discord / Spotify, try to stick to a browser. I would also heavily recommend using flatpaks in combination with FlatSeal to manage permissions and only allow the application to do what you want. 

Ideally, everything you install through the help of an AUR helper or just pacman (Like ani-cli), I would highly suggest creating an AppArmor profile to sandbox that specific application. Not for everything of course, but just applications you use regularly. 

But of course, nothing is perfect. Although I'd like to believe that this guide will you get about 90% of the way there. There is so much you can do, like reading NSA kernel hardening documents, how certain vulnerabilities work, installing custom firmware like CoreBoot, etc etc. But a lot of this hits diminishing returns rather quickly. 

But whatever you do, stay sane, don't be dumb :)



